const firstName = document.getElementById("firstName");
const lastName = document.getElementById("lastName");
const correctAnswers = {
	"question-1": "wolf",
	"question-2": "prisoner"
};

const submitButton = document.querySelector("#submit-button");
const totalScore = document.querySelector("#total-score");

submitButton.addEventListener("click", () => {
	let score = 0;
	for (const [questionId, answer] of Object.entries(correctAnswers)) {
		const selectedAnswer = document.querySelector(`#${questionId}`).value;
	const messageElement =	document.querySelector(`#${questionId}-message`);

	if (selectedAnswer === answer) {
		messageElement.innerText = "Your Answer is Correct!";
		messageElement.style.color = "green";
		score++;
	} else {
		messageElement.innerText = "Your Answer is Incorrect!";
		messageElement.style.color = "red";
	}
	}
	totalScore.innerText = `Good Day! ${firstName.value} ${lastName.value} You got ${score} out of ${Object.keys(correctAnswers).length}.`;

	alert(totalScore.innerText);
});
